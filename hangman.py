def unknow_word(word):
    for i in range(0, len(word)):
        word = word.replace(word[i], "_")
    return word
def replace_str_index(text,index=0,replacement=''):
    return f"{text[:index]}{replacement}{text[index+1:]}"
def gibbet(try_num):
    if try_num == 1:
        print("""            





        /\\         """)
    elif try_num == 2:
        print("""        |          
        |
        |
        |
        |
        |
        /\\         """)
    elif try_num == 3:
        print("""        |---------        
        |
        |
        |
        |
        |
        /\\        """)
    elif try_num == 4:
        print("""        |---------        
        |        |
        |
        |
        |
        |
        /\\        """)
    elif try_num == 5:
        print("""        |---------        
        |        |
        |        O
        |
        |
        |
        /\\        """)
    elif try_num == 6:
        print("""        |---------        
        |        |
        |        O
        |        |
        |
        |
        /\\        """)
    elif try_num == 7:
        print("""        |---------        
        |        |
        |        O
        |       /|
        |
        |
        /\\        """)
    elif try_num == 8:
        print("""        |---------        
        |        |
        |        O
        |       /|\\
        |
        |
        /\\        """)
    elif try_num == 9:
        print("""        |---------        
        |        |
        |        O
        |       /|\\
        |       /
        |
        /\\        """)
    elif try_num == 10:
        print("""        |---------        
        |        |
        |        O
        |       /|\\
        |       / \\
        |
        /\\        """)
x = 0
letters = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"}
user_word = input("P1 Enter your word(use only letters from alphabet): ").lower()
exposed_character = unknow_word(user_word)
used_letters = []
for k in range(0, len(exposed_character)):
    if user_word[k] == " ":
        exposed_character = replace_str_index(exposed_character, k, " ")
print(exposed_character)
while "_" in exposed_character:
    guess_character = input("P2 Guess charakter in word: ")
    guess_character = guess_character.lower()
    used_letters.append(guess_character)
    if guess_character in letters:
        if guess_character in user_word:
            for i in range(0, len(user_word)):
                if user_word[i] == guess_character:
                    exposed_character = replace_str_index(exposed_character, i, guess_character)
            print("Correct!")
            print("\n")
            print(exposed_character)
            if "_" not in exposed_character:
                print("Congratulation P1, you guessed the word!")
                break
        else:
            gibbet(1 + x)
            x += 1
            if x == 10:
                print("P2 You lost :c")
                break
            print("Wrong letter. Try again")
            print("\n")
            print(exposed_character)
    else:
        print(f"{guess_character} is not character from alphabet")
    print("Letters used: ", end="")
    print(*used_letters, sep=", ")
print("Thank's for the game c:")
